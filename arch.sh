#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

sgdisk --zap-all /dev/sdb
wipefs -a /dev/sdb

timedatectl set-ntp true

sgdisk -og /dev/sdb
sgdisk -n 1:0:+600MiB -c 1:"EFIBOOT" -t 1:ef00 /dev/sdb
sgdisk -n 2:0:+35GiB -c 2:"root" -t 2:8304 /dev/sdb
sgdisk -n 3:0:+8GiB -c 3:"/tmp" -t 3:8300 /dev/sdb
sgdisk -n 4:0:+30GiB -c 4:"/home" -t 4:8302 /dev/sdb

mkfs.fat -F32 /dev/sdb1
mkfs.ext4 /dev/sdb2 -F
mkfs.ext4 /dev/sdb3 -F
mkfs.ext4 /dev/sdb4 -F

mount /dev/sdb2 /mnt
mkdir -p /mnt/boot
mount /dev/sdb1 /mnt/boot
mkdir -p /mnt/tmp
mount /dev/sdb3 /mnt/tmp
mkdir -p /mnt/home
mount /dev/sdb4 /mnt/home

echo
echo "#############################################"
echo "Disk(s) partitioned and mounted."
echo "Starting pacstrap..."
echo "#############################################"
echo

pacman --noconfirm -Sy archlinux-keyring >/dev/null 2>&1

pacstrap /mnt base base-devel linux linux-firmware intel-ucode networkmanager wget rsync

genfstab -U /mnt >> /mnt/etc/fstab

echo
echo "#############################################"
echo "Entering chroot..."
echo "#############################################"
echo

curl https://gitlab.com/xnay/archlap/-/raw/master/chroot.sh > /mnt/chroot.sh
arch-chroot /mnt bash ./chroot.sh && rm /mnt/chroot.sh

umount -R /mnt

